﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Linq;

namespace Drawing
{
    // implementacja kompozytu
    [Serializable()]
    public class ShapeGroup : IShape
    {
        private List<IShape> shapes = new List<IShape>();

        public void Add(IShape shape)
        {
            shapes.Add(shape);
        }

        public void Remove(IShape shape)
        {
            shapes.Remove(shape);
        }

        #region IShape Members

        public void Draw()
        {
            foreach (IShape s in shapes)
                s.Draw();
        }

        public void Move(int dx, int dy)
        {
            foreach (IShape s in shapes)
                s.Move(dx, dy);
        }

        public IShape Clone()
        {
            IShape clonedShape = null;
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();

                formatter.Serialize(stream, this);
                stream.Seek(0, SeekOrigin.Begin);
                clonedShape = formatter.Deserialize(stream) as IShape;
            }

            return clonedShape;
        }

        public class Factory : ShapeFactory
        {

            protected override IShape CreateShape(params object[] args)
            {
                XElement groupElement = (XElement)args[0];

                ShapeGroup shapeGroup = new ShapeGroup();

                foreach(var subElement in groupElement.Elements("Shape"))
                {
                    string id = subElement.Attribute("Id").Value;
                    IShape shape = ShapeFactory.Create(id, subElement);
                    shapeGroup.Add(shape);
                }

                return shapeGroup;
            }
        }

        #endregion
    }
}
