﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Drawing;
using System.Xml.Linq;

namespace Drawing
{

    [Serializable]
    public class ImageProxy : Shape
    {
        private string _path;

        public string Path
        {
            get { return _path; }
            set
            {
                if (_image != null)
                    _image.Path = value;
                _path = value;
            }
        }

        private Image _image;

        public ImageProxy(int x, int y, string path) 
        {
            AddPoint(new Point(x, y));
            _path = path;
        }

        public override void Draw()
        {
            if (_image == null)
                _image = new Image(GetPoint(0).X, GetPoint(0).Y, _path);

            _image.Draw();
        }

        public class Factory : ShapeFactory
        {
            protected override IShape CreateShape(params object[] args)
            {
                XElement imageElement = (XElement)args[0];
                int x = int.Parse(imageElement.Element("Point").Element("X").Value);
                int y = int.Parse(imageElement.Element("Point").Element("Y").Value);
                string path = imageElement.Element("Path").Value;

                return new ImageProxy(x, y, path);
            }
        }
    }
}
