﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Structural.Proxy.TheoryCode
{
    /// <summary>
    /// Proxy Design Pattern.
    /// </summary>
    class MainApp
    {
        /// <summary>
        /// Entry point into console application.
        /// </summary>
        static void Main()
        {
            // Create proxy and request a service
            ISubject subject = new Proxy();

            Console.ReadKey();

            subject.Request();

            // Wait for user
            Console.Read();
        }
    }

    // "Subject" 
    interface ISubject
    {
        void Request();
    }

    // "RealSubject" 
    class RealSubject : ISubject
    {
        public RealSubject()
        {
            Console.WriteLine("Constructor RealSubject");
        }

        public void Request()
        {
            Console.WriteLine("Called RealSubject.Request()");
        }
    }
	
    // "Proxy" 
    class Proxy : ISubject
    {
        RealSubject realSubject;

        public Proxy()
        {
            Console.WriteLine("Constructor Proxy");
        }

        public void Request()
        {
            // Use 'lazy initialization'
            if (realSubject == null)
            {
                realSubject = new RealSubject();
            }

            realSubject.Request();
        }
    }
}
