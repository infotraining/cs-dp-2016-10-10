﻿namespace BankAccount.Model
{
    public enum AccountState
    {
        Normal = 1,
        Overdraft = 2
    };
}

