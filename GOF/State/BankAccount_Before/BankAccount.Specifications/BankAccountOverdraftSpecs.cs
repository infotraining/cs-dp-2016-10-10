using System;
using BankAccount.Model;
using Machine.Specifications;

namespace BankAccount.Specifications
{
    public class BankAccountOverdraftSpecs
    {
        [Behaviors]
        public class BankAccountInOverdraftState
        {
            It should_return_status_as_overdraft = () => _account.Status.ShouldEqual(AccountState.Overdraft);

            protected static Model.BankAccount _account;
        }

        public class with_bank_account_in_overdraft_state
        {
            protected static Model.BankAccount _account;
            protected static decimal _initialDebit = -100.0M;

            private Establish context = () =>
            {
                _account = new Model.BankAccount(1, _initialDebit);
            };           
        }

        [Subject(typeof (Model.BankAccount), "BankAccount in overdraft state")]
        public class When_new_account_is_created_with_balance_less_than_zero : with_bank_account_in_overdraft_state
        {
            private It should_return_status_overdraft = () => _account.Status.ShouldEqual(AccountState.Overdraft);
        }


        [Subject(typeof(Model.BankAccount), "BankAccount in overdraft state")]
        public class When_making_withdrawal : with_bank_account_in_overdraft_state
        {
            static Exception _exception;

            private Because of = () => _exception = Catch.Exception(() => _account.Withdraw(1.0M));

            private Behaves_like<BankAccountInOverdraftState> _account_in_overdraft_state;

            private It should_fail = () => _exception.ShouldBeOfExactType<InsufficientFunds>();

            private It should_have_failure_description =
                () => _exception.Message.ShouldContain("Insufficient funds on account #1");        
        }

        [Subject(typeof (Model.BankAccount), "BankAccount in overdraft state")]
        public class When_making_deposit_less_than_debit : with_bank_account_in_overdraft_state
        {
            private Because of = () => _account.Deposit(50.0M);

            private Behaves_like<BankAccountInOverdraftState> account_in_overdraft_state; 

            private It should_increase_the_balance_with_a_proper_amount = () => _account.Balance.ShouldEqual(-50.0M);
        }

        [Subject(typeof(Model.BankAccount), "BankAccount in overdraft state")]
        public class When_making_deposit_greater_than_debit : with_bank_account_in_overdraft_state
        {
            private Because of = () => _account.Deposit(150.0M);

            private It should_increase_the_balance_with_a_proper_amount = () => _account.Balance.ShouldEqual(50.0M);

            private It should_return_status_normal = () => _account.Status.ShouldEqual(AccountState.Normal);
        }

        [Subject(typeof(Model.BankAccount), "BankAccount in overdraft state")]
        public class When_paying_interests : with_bank_account_in_overdraft_state
        {
            private Because of = () => _account.PayInterest();

            private Behaves_like<BankAccountInOverdraftState> account_in_overdraft_state;

            private It should_increase_the_debit_with_interests = () => _account.Balance.ShouldEqual(-120.0M);
        }
    }
}