﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace DP.Behavioral.State.Example
{
    interface ITurnstileState
    {
        ITurnstileState Coin(TurnstileApi t);
        ITurnstileState Pass(TurnstileApi t);
    }

    class TurstileLocked : ITurnstileState
    {
        #region ITurnstilleState Members

        public ITurnstileState Coin(TurnstileApi t)
        {
            t.Unlock();

            return Turnstile.UnlockedState;
        }

        public ITurnstileState Pass(TurnstileApi t)
        {
            t.Alarm();

            return this;
        }

        #endregion
    }

    class TurstileUnlocked : ITurnstileState
    {
        #region ITurnstilleState Members

        public ITurnstileState Coin(TurnstileApi t)
        {
            t.ThankYou();

            return this;
        }

        public ITurnstileState Pass(TurnstileApi t)
        {            
            t.Lock();

            return Turnstile.LockedState;
        }

        #endregion
    }

    class Turnstile
    {
        internal static ITurnstileState LockedState = new TurstileLocked();
        internal static ITurnstileState UnlockedState = new TurstileUnlocked();

        ITurnstileState _state;
        private readonly TurnstileApi _turnstileApi;

        public Turnstile()
        {
            _state = new TurstileLocked();
            _turnstileApi = new TurnstileApi();
        }

        public void Coin()
        {
            _state = _state.Coin(_turnstileApi);
        }

        public void Pass()
        {
            _state = _state.Pass(_turnstileApi);
        }
    }

    class Program
    {
        public static void Main()
        {
            Turnstile t = new Turnstile();

            t.Coin();
            t.Pass();
            t.Pass();
            t.Coin();
            t.Coin();
            t.Pass();
            t.Pass();
        }
    }
}
