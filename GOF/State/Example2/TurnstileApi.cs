using System;

namespace DP.Behavioral.State.Example
{
    class TurnstileApi
    {
        public TurnstileApi()
        {
        }

        public void Lock()
        {
            Console.WriteLine("LOCKED");
        }

        public void Unlock()
        {
            Console.WriteLine("UNLOCKED");
        }

        public void Alarm()
        {
            Console.WriteLine("ALARM");
        }

        public void ThankYou()
        {
            Console.WriteLine("THANK YOU");
        }
    }
}