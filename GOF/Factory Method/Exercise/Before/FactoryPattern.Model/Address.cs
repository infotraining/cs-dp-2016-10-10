﻿namespace FactoryPattern.Model
{
    public class Address
    {
        public string CountryCode { get; set; }
    }
}
