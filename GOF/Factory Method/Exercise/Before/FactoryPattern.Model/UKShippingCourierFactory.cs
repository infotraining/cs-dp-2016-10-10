﻿using System;
using System.Collections.Generic;

namespace FactoryPattern.Model
{
    public class SpecifiedShippingCreator
    {
        public Predicate<Order> IsSatisfied { get; set; }
        public Func<IShippingCourier> CreateShippingCourier { get; set; } 
    }

    public static class UKShippingCourierFactory
    {
        private static SpecifiedShippingCreator defautCreator = new SpecifiedShippingCreator
        {
            IsSatisfied = (o) => true,
            CreateShippingCourier = () => new RoyalMail()
        };

        static UKShippingCourierFactory()
        {
            
            _creators.Add(new SpecifiedShippingCreator() { IsSatisfied = (o) => ((o.TotalCost > 100) || (o.WeightInKG > 5)), CreateShippingCourier = () => new DHL() });
        }

        private static List<SpecifiedShippingCreator> _creators = new List<SpecifiedShippingCreator>();
             

        public static IShippingCourier CreateShippingCourier(Order order)
        {
            foreach (var c in _creators)
            {
                if (c.IsSatisfied(order))
                    return c.CreateShippingCourier();
            }

            return defautCreator.CreateShippingCourier();
        }
    }
}
