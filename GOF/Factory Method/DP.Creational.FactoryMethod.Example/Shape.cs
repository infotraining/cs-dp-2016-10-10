﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Drawing
{
    public interface IShape
    {
        void Draw();
        void Move(int dx, int dy);
    }

    public abstract class Shape : IShape
    {
        protected List<Point> Points { get; set; } = new List<Point>();

        protected int NumberOfPoints()
        {
            return Points.Count;
        }

        protected void AddPoint(Point pt)
        {
            Points.Add(pt);
        }

        #region IShape Members

        public abstract void Draw();

        public virtual void Move(int dx, int dy)
        {
            for (int i = 0; i < Points.Count; ++i)
            {
                Points[i] = new Point(Points[i].X + dx, Points[i].Y + dy);
            }
        }

        #endregion
    }

    public interface IShapeCreator
    {
        IShape CreateShape(params object[] args);        
    }

    public class ShapeFactory
    {
        private IDictionary<string, IShapeCreator> _creators = new Dictionary<string, IShapeCreator>();

        public IShape Create(string id, params object[] args)
        {
            return _creators[id].CreateShape(args);
        }

        public void Register(string id, IShapeCreator creator)
        {
            _creators.Add(id, creator);
        }

        public void Unregister(string id)
        {
            _creators.Remove(id);
        }
    }
}
