﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace DP.Structural.Adapter.Pluggable
{
    public class XmlTreeContentProvider : ITreeContentProvider<XElement>
    {
        #region ITreeContentProvider<XElement> Members

        public IEnumerable<XElement> GetChildren(XElement node)
        {
            return node.Elements();
        }

        public XElement GetParent(XElement node)
        {
            return node.Parent;
        }

        public bool HasChildren(XElement node)
        {
            return node.Elements().Count() > 0;
        }

        public string GetText(XElement node)
        {
            StringBuilder strBuilder = new StringBuilder();

            strBuilder.Append(node.Name + " ");

            foreach (var attr in node.Attributes())
                strBuilder.Append(attr.Name + ": " + attr.Value + "; ");

            if (!node.HasElements)
                strBuilder.Append(node.Value);

            return strBuilder.ToString();
        }

        #endregion
    }
}
