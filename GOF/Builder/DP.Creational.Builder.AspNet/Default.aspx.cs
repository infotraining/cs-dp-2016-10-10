﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SurveyBuilder.App_Code;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        WebFormsSurveyBuilder builder = new WebFormsSurveyBuilder();
        SurveyXmlParser parser = new SurveyXmlParser(builder);
        parser.BuildSurvey(HttpContext.Current.Server.MapPath("~/App_Data") + "/survey.xml");

        pnlSurvey.Controls.Add(builder.GetSurvey());
    }

}