﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Creational.Builder.Example
{
    public class Car
    {
        internal string Engine { get; set; }
        internal string Gearbox { get; set; }
        internal int AirbagsCount { get; set; }
        internal string Aircondition { get; set; }
        internal List<string> Wheels { get; set; } = new List<string>();

        public override string ToString()
        {
            StringBuilder description = new StringBuilder();
            description.Append(string.Format("Car(engine = {0}, gearbox = {1}, airbags_count = {2}, aircondition = {3}, wheels = [ ",
                Engine, Gearbox, AirbagsCount, (string.IsNullOrEmpty(Aircondition)) ? "None" : Aircondition));

            foreach (var wheel in Wheels)
            {
                description.Append(wheel).Append(" ");
            }

            description.Append("])");

            return description.ToString();
        }
    }

    public interface ICarBuilder
    {
        ICarBuilder BuildEngine();
        ICarBuilder BuildGearbox();
        ICarBuilder BuildAirbags();
        ICarBuilder BuildAircondition();
        ICarBuilder BuildWheel();
    }

    public abstract class CarBuilder : ICarBuilder
    {
        protected Car Car { get; set; }

        protected CarBuilder()
        {
            Car = new Car();         
        }

        public abstract ICarBuilder BuildEngine();
        public abstract ICarBuilder BuildGearbox();
        public abstract ICarBuilder BuildAirbags();
        public abstract ICarBuilder BuildAircondition();
        public abstract ICarBuilder BuildWheel();

        public Car GetCar()
        {
            return Car;
        }
    }

    public class EconomyCarBuilder : CarBuilder
    {
        public override ICarBuilder BuildEngine()
        {
            Car.Engine = "petrol 1.1";
            return this;
        }

        public override ICarBuilder BuildGearbox()
        {
            Car.Gearbox = "manual 5";
            return this;
        }

        public override ICarBuilder BuildAirbags()
        {
            Car.AirbagsCount = 1;
            return this;
        }

        public override ICarBuilder BuildAircondition()
        {
            return this;
        }

        public override ICarBuilder BuildWheel()
        {
            Car.Wheels.Add("steel 14'");
            return this;
        }
    }

    public class PremiumCarBuilder : CarBuilder
    {
        public override ICarBuilder BuildEngine()
        {
            Car.Engine = "petrol 4.2";

            return this;
        }

        public override ICarBuilder BuildGearbox()
        {
            Car.Gearbox = "manual 6";

            return this;
        }

        public override ICarBuilder BuildAirbags()
        {
            Car.AirbagsCount = 10;

            return this;
        }

        public override ICarBuilder BuildAircondition()
        {
            Car.Aircondition = "automatic 2 zones";

            return this;
        }

        public override ICarBuilder BuildWheel()
        {
            Car.Wheels.Add("gold 17'");

            return this;
        }
    }


    public interface IDirector
    {
        ICarBuilder CarBuilder { get; set; }
        void Construct();
    }

    public class Director : IDirector
    {
        public ICarBuilder CarBuilder { get; set; }

        public Director(ICarBuilder carBuilder)
        {
            CarBuilder = carBuilder;
        }

        public virtual void Construct()
        {
            CarBuilder.BuildEngine();
            CarBuilder.BuildGearbox();
            CarBuilder.BuildAirbags();
            CarBuilder.BuildAircondition();

            for (int i = 0; i < 4; ++i)
                CarBuilder.BuildWheel();
        }
    }

    public class RacingCarDirector : Director
    {
        public RacingCarDirector(ICarBuilder carBuilder) : base(carBuilder)
        {
        }

        public override void Construct()
        {
            CarBuilder.BuildEngine();
            CarBuilder.BuildGearbox();
            
            for (int i = 0; i < 4; ++i)
                CarBuilder.BuildWheel();
        }
    }

    class Program
    {
        public static void Main()
        {
            PremiumCarBuilder carBuilder = new PremiumCarBuilder();

            IDirector director = new RacingCarDirector(carBuilder);
            director.Construct();

            Car car = carBuilder.GetCar();

            Console.WriteLine(car.ToString());
        }
    }
}