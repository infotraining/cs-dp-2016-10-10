﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Drawing;
using System.Drawing;

namespace DP.Structural.Bridge.Example
{
    public class WinFormsShapeImpl : IShapeImpl
    {
        Graphics g;

        public WinFormsShapeImpl(Graphics graphics)
        {
            g = graphics;
        }

        public void DrawLine(Drawing.Point start, Drawing.Point end)
        {
            g.DrawLine(Pens.Black, start.X, start.Y, end.X, end.Y);
        }

        public void DrawEllipse(Drawing.Point start, Drawing.Point end)
        {
            g.DrawEllipse(Pens.Black, start.X, start.Y, end.X, end.Y);
        }
    }
}
