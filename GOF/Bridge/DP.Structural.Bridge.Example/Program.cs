﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Drawing;

namespace DP.Structural.Bridge.Example
{
    static class Program
    {
        static void Init()
        {
            ShapeFactory.Register("Circle", new Circle.Factory());
            ShapeFactory.Register("Rectangle", new Rectangle.Factory());
            ShapeFactory.Register("Line", new Line.Factory());
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Init();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
