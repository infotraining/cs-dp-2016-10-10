﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Drawing
{
    public class Circle : Shape
    {
        public int Radius { get; set; }

        public Circle(int x, int y, int radius)
        {
            AddPoint(new Point(x, y));
            Radius = radius;
        }

        public override void Draw()
        {
            Point topLeft = new Point(GetPoint(0).X - Radius/2, GetPoint(0).Y - Radius/2);
            Point rightBottom = new Point(GetPoint(0).X + Radius/2, GetPoint(0).Y + Radius/2);
            Implementation.DrawEllipse(topLeft, rightBottom);
        }

        public class Factory : ShapeFactory
        {
            protected override IShape CreateShape(params object[] args)
            {
                XElement circleElement = (XElement)args[0];
                int x = int.Parse(circleElement.Element("Point").Element("X").Value);
                int y = int.Parse(circleElement.Element("Point").Element("Y").Value);
                int radius = int.Parse(circleElement.Element("Radius").Value);

                return new Circle(x, y, radius);
            }
        }
    }
}
