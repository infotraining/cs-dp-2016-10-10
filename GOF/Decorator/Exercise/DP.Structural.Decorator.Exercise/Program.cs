﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DP.Structural.Decorator.Exercise
{
    class CoffeeBuilder
    {
        private Coffee _coffee;

        public CoffeeBuilder CreateBase<TBase>() where TBase : Coffee, new()
        {
            _coffee = new TBase();

            return this;
        }

        public CoffeeBuilder Add<TCondiment>() where TCondiment : CoffeeDecorator
        {
            CoffeeDecorator decorated = (CoffeeDecorator)Activator.CreateInstance(typeof(TCondiment), nonPublic:true);
            decorated.Coffee = _coffee;
            _coffee = decorated;

            return this;
        }

        public Coffee GetCoffee()
        {
            return _coffee;
        }
    }

    class Program
    {
        static void Drink(Coffee coffee)
        {
            Console.WriteLine("Coffee: {0} - Price: {1:c}", coffee.GetDescription(), coffee.GetTotalPrice());
            coffee.Prepare();

        }

        static void Main(string[] args)
        {
           Coffee myCoffee = new Whipped(new Whisky(new ExtraEspresso( new Espresso())));

            CoffeeBuilder cb = new CoffeeBuilder();
            cb.CreateBase<Espresso>().Add<ExtraEspresso>().Add<Whisky>().Add<Whipped>();

            myCoffee = cb.GetCoffee();

           Drink(myCoffee);

            Console.ReadKey();
        }
    }
}
