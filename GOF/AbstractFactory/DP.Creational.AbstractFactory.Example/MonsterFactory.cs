﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Creational.AbstractFactory.Example
{
    // interface of enemy factory
    public interface MonsterFactory
    {
        Soldier CreateSoldier();
        Monster CreateMonster();
        SuperMonster CreateSuperMonster();
    }

    // Enemy factory - level 1
    public class SillyMonsterFactory : MonsterFactory
    {
        #region MonsterFactory Members

        public Soldier CreateSoldier()
        {
            return new SillySoldier();
        }

        public Monster CreateMonster()
        {
            return new SillyMonster();
        }

        public SuperMonster CreateSuperMonster()
        {
            return new SillySuperMonster();
        }

        #endregion
    }

    // Enemy factory - level 2
    public class BadMonsterFactory : MonsterFactory
    {
        #region MonsterFactory Members

        public Soldier CreateSoldier()
        {
            return new BadSoldier();
        }

        public Monster CreateMonster()
        {
            return new BadMonster();
        }

        public SuperMonster CreateSuperMonster()
        {
            return new BadSuperMonster();
        }

        #endregion
    }
}
