﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Common;
using System.Configuration;

namespace DP.Creational.AbstractFactory.Example.NET
{
  public partial class MainForm : Form
  {
    private DbProviderFactory factory;
    private DbConnection connection;
    private DbDataAdapter adapter;
    private DbCommand command;

    public MainForm()
    {
      InitializeComponent();

      // inicjalizacja polaczenia z baza
      try
      {
          string providerName = ConfigurationManager.AppSettings["provider"];
          string connString = ConfigurationManager.ConnectionStrings["northwindCS"].ConnectionString;

          // utworzenie fabryki
          factory = DbProviderFactories.GetFactory(providerName);

          // utworzenie polaczenia
          connection = factory.CreateConnection();
          connection.ConnectionString = connString;
      }
      catch (Exception ex)
      {
          MessageBox.Show(ex.Message, "Error");
      }
    }

    private void btnExecute_Click(object sender, EventArgs e)
    {
      string strSql = txtSql.Text;

      if (string.IsNullOrEmpty(strSql))
      {
        MessageBox.Show("Enter an sql expression...", "Northwind...");
        return;
      }

      try
      {
        // wykonanie polecenia sql
        command = factory.CreateCommand();
        command.Connection = connection;
        command.CommandText = strSql;

        adapter = factory.CreateDataAdapter();
        adapter.SelectCommand = command;

        DataTable results = new DataTable();
        adapter.Fill(results);

        dgvResults.DataSource = results;
      }
      catch (Exception excpt)
      {
        MessageBox.Show(excpt.Message, "Error");
      }
    }
  }
}
