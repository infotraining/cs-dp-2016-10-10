﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DP.Behavioral.Command.Example;

namespace DP.Behavioral.Command.Example
{
    public interface ICommand
    {
        void Execute();
        void Undo();
        ICommand Clone();
    }

    public abstract class CommandBase : ICommand
    {
        public virtual ICommand Clone()
        {
            return (ICommand)MemberwiseClone();
        }

        public abstract void Execute();
        public abstract void Undo();
    }

    public abstract class UndoableCommand : CommandBase
    {
        private ICommandHistory _commandHistory;

        public UndoableCommand(ICommandHistory commandHistory)
        {
            _commandHistory = commandHistory;
        }

        protected ICommandHistory CommandHistory
        {
            get { return _commandHistory; }
        }
    }


    public class CopyCommand : CommandBase
    {
        private TextBox receiver;

        public CopyCommand(TextBox receiver)
        {
            this.receiver = receiver;
        }

        #region ICommand Members

        public override void Execute()
        {
            receiver.Copy();
        }

        #endregion

        public override void Undo()
        {
        }
    }

    public class PasteCommand : UndoableCommand
    {
        string before;

        private TextBox receiver;

        public PasteCommand(TextBox receiver, ICommandHistory commandHistory)
            : base(commandHistory)
        {
            this.receiver = receiver;
        }

        #region ICommand Members

        public override void Execute()
        {
            before = receiver.Text;
            CommandHistory.Push(this.Clone());
            receiver.Paste();
        }

        #endregion

        public override void Undo()
        {
            receiver.Text = before;
        }
    }

    public class ToUpperCommand : UndoableCommand
    {
        private TextBox receiver;
        private string selectedText;
        private int selectionStarts;
        private int selectionLength;

        public ToUpperCommand(TextBox receiver, ICommandHistory commandHistory)
            : base(commandHistory)
        {
            this.receiver = receiver;
        }

        #region ICommand Members

        public override void Execute()
        {
            selectedText = receiver.SelectedText;
            selectionStarts = receiver.SelectionStart;
            selectionLength = receiver.SelectionLength;
            receiver.Text = receiver.Text.Remove(selectionStarts, selectionLength);
            receiver.Text = receiver.Text.Insert(selectionStarts, selectedText.ToUpper());
        }

        #endregion

        public override void Undo()
        {
            // TODO
        }
    }

    public class RemoveCommand : UndoableCommand
    {
        private TextBox receiver;
        private string selectedText;
        private int selectionStarts;
        private int selectionLength;

        public RemoveCommand(TextBox receiver, ICommandHistory commandHistory)
            : base(commandHistory)
        {
            this.receiver = receiver;
        }

        #region ICommand Members

        public override void Execute()
        {
            selectionStarts = receiver.SelectionStart;
            selectionLength = receiver.SelectionLength;

            if (selectionLength > 0)
            {
                selectedText = receiver.SelectedText;
                CommandHistory.Push(this.Clone());
                receiver.Text = receiver.Text.Remove(selectionStarts, selectionLength);
            }
        }

        #endregion

        public override void Undo()
        {
            receiver.Text = receiver.Text.Insert(selectionStarts, selectedText);
        }
    }

    // TODO
    //public class ToLowerCommand : ICommand
    //{
    //}

    public class UndoCommand : UndoableCommand
    {
        public UndoCommand(ICommandHistory commandHistory)
            : base(commandHistory)
        {

        }

        public override void Execute()
        {
            ICommand lastCmd = CommandHistory.Pop();
            lastCmd.Undo();
        }

        public override void Undo()
        {
        }
    }
}