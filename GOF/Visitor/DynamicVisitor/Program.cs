﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DynamicVisitor
{
    class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        // The Friends collection may contain Customers & Employees:
        public readonly IList<Person> Friends = new List<Person>();
    }

    class Customer : Person { public decimal CreditLimit { get; set; } }
    class Employee : Person { public decimal Salary { get; set; } }

    class ToXElementPersonVisitor
    {
        public XElement DynamicVisit(Person p) => Visit((dynamic)p);

        XElement Visit(Person p)
        {
            return new XElement("Person",
              new XAttribute("Type", p.GetType().Name),
              new XElement("FirstName", p.FirstName),
              new XElement("LastName", p.LastName),
              p.Friends.Select(f => DynamicVisit(f))
            );
        }

        XElement Visit(Customer c)    // Specialized logic for customers
        {
            XElement xe = Visit((Person)c);    // Call "base" method
            xe.Add(new XElement("CreditLimit", c.CreditLimit));
            return xe;
        }

        XElement Visit(Employee e)    // Specialized logic for employees
        {
            XElement xe = Visit((Person)e);   // Call "base" method
            xe.Add(new XElement("Salary", e.Salary));
            return xe;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            var cust = new Customer
            {
                FirstName = "Zenon",
                LastName = "Kowalski",
                CreditLimit = 1230
            };

            cust.Friends.Add(
              new Employee { FirstName = "Anna", LastName = "Nowak", Salary = 50000 }
            );

            Console.WriteLine(new ToXElementPersonVisitor().DynamicVisit(cust));
        }
    }
}
