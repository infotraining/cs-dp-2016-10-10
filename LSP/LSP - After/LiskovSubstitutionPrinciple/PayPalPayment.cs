﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiskovSubstitutionPrinciple.Mocks;

namespace LiskovSubstitutionPrinciple
{
    public class PayPalPayment : PaymentServiceBase
    {
        public PayPalPayment(string accountName, string password)
        {
            AccountName = accountName;
            Password = password;
        }

        public string AccountName { get; set; }
        public string Password { get; set; }

        public override RefundResponse Refund(decimal amount, string transactionId)
        {
            RefundResponse refundResponse = new RefundResponse();

            MockPayPalWebService payPalWebService = new MockPayPalWebService();

            string token = payPalWebService.ObtainToken(AccountName, Password);

            refundResponse.Message = payPalWebService.MakeRefund(amount, transactionId, token);
            refundResponse.Success = refundResponse.Message.Contains("Auth");

            return refundResponse;
        }
    }
}
